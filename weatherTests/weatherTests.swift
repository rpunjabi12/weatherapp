//
//  weatherTests.swift
//  weatherTests
//
//  Created by Renu Punjabi on 6/25/15.
//  Copyright (c) 2015 Cocoa Team. All rights reserved.
//

import UIKit
import XCTest
import Nimble

class weatherTests: XCTestCase {
   
    let vc = ViewController()
    let weatherManager = WeatherManager()
    
    func test_convertKelvinToFarenhite() {
        expect(self.weatherManager.convertKelvinToFarenhite(temp: "300.0")).to(equal("80.33"))
    }
    
    func test_convertKelvinToCelsius() {
        expect(self.weatherManager.convertKelvinToCelcius(temp: "300.0")).to(equal("26.85"))
    }
  
    
}
