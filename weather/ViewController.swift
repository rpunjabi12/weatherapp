//
//  ViewController.swift
//  weather
//
//  Created by Renu Punjabi on 6/25/15.
//  Copyright (c) 2015 Cocoa Team. All rights reserved.
//

import UIKit

class ViewController: UIViewController, WeatherManagerDelegate, UITextFieldDelegate {
    
    @IBOutlet weak var temperatureLabel: UILabel!
    let weatherManager = WeatherManager()

    @IBAction func celsiusButtonClicked(sender: AnyObject) {
        let str = weatherManager.convertKelvinToCelcius(temp: weatherManager.kelvin)
        temperatureLabel.text = str as String
    }
    @IBAction func farenhietButtonClicked(sender: AnyObject) {
       let str = weatherManager.convertKelvinToFarenhite(temp: weatherManager.kelvin)
        temperatureLabel.text = str as String
    }
    @IBAction func kelvinButtonClicked(sender: AnyObject) {
        temperatureLabel.text = weatherManager.kelvin

    }
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        weatherManager.weatherDelegate = self
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

//MARK: WeatherManger delegate method
    func populateTemperatureLabel(#temp: String) {
        temperatureLabel.text = temp
    }
    
//MARK: UITextFieldDelegate method
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        weatherManager.getWeatherByCityOrZipCode(city: textField.text, zipCode: textField.text)
        return true
    }
   

}

//    @IBAction func optionsButtonClicked(sender: AnyObject) {
//
//        let storyboard = UIStoryboard(name: "Main", bundle: nil)
//
//        let vc: MenuOptionsCollectionView = storyboard.instantiateViewControllerWithIdentifier("menuCollectionView") as! MenuOptionsCollectionView
//
//        self.modalPresentationStyle = UIModalPresentationStyle.CurrentContext
//
//        presentViewController(vc, animated: false, completion: nil)
//    }

