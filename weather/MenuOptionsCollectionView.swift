//
//  MenuOptionsCollectionView.swift
//  weather
//
//  Created by Renu Punjabi on 6/29/15.
//  Copyright (c) 2015 Cocoa Team. All rights reserved.
//

import Foundation
import UIKit

class MenuOptionsCollectionView: UICollectionViewController {
    
    let optionsArray = ["Kelvin", "Farenheit", "Celsious"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.clearColor()
        
//        self.collectionView?.registerClass([UICollectionViewCell class], forCellWithReuseIdentifier: "OptionsCell")
    }
    
    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        var cell = collectionView.dequeueReusableCellWithReuseIdentifier("OptionsCell", forIndexPath: indexPath) as! UICollectionViewCell
        
        
        let button  = UIButton()
        button.titleLabel?.text = optionsArray[indexPath.row]
        button.addTarget(self, action: "buttonClicked", forControlEvents: UIControlEvents.TouchUpInside)
        button.backgroundColor = UIColor.redColor()
        cell.contentView.addSubview(button)
        cell.contentView.backgroundColor = UIColor.greenColor()

        
        return cell
    }
    
    
//    override func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
//        <#code#>
//    }
    
    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        println("number of rows = \(optionsArray.count)")
        return optionsArray.count
    }
    

}
