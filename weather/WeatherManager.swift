//
//  WeatherManager.swift
//  weather
//
//  Created by Renu Punjabi on 6/25/15.
//  Copyright (c) 2015 Cocoa Team. All rights reserved.
//

import Foundation

protocol WeatherManagerDelegate: class {
    func populateTemperatureLabel(#temp: String)
}

class WeatherManager {
    
    weak var weatherDelegate: WeatherManagerDelegate?
    
    var kelvin = ""
    var celsius = ""
    var farenhiet = ""
    
    func getWeatherByCityOrZipCode(#city: String, zipCode: String?) {
        
        var weatherURLStr = ""
        
        if zipCode != nil {
            weatherURLStr = "http://api.openweathermap.org/data/2.5/weather?zip=" + zipCode!
            
        }else{
            weatherURLStr = "http://api.openweathermap.org/data/2.5/weather?q=" + city
        }
        
        let url = NSURL(string: weatherURLStr)
        
        let task = NSURLSession.sharedSession().dataTaskWithURL(url!) {(data, response, error) in
            var dict: AnyObject = NSJSONSerialization.JSONObjectWithData(data, options: nil, error: nil)!
            let dictionary = dict as? NSDictionary
            let temperatureDict = dictionary!["main"] as! NSDictionary
            
            self.kelvin = temperatureDict["temp"]!.description

//            println("Kelvin = \(self.kelvin)")
        
            self.cacheWeatherJSONFile(data)
            
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                self.callDelegateToPopulateTemperatureDisplayLabel()
            })
        }
        
        task.resume()
    }
    
    func callDelegateToPopulateTemperatureDisplayLabel() {
        self.weatherDelegate?.populateTemperatureLabel(temp: self.kelvin)
    } 
    
    func convertKelvinToCelcius(#temp: NSString) ->NSString {
        let t = temp.floatValue
        let celsius = t - 273.15
        let str = celsius.description
        println("str = \(str)")
        self.celsius = str
        return str
    }
    
    func convertKelvinToFarenhite(#temp: NSString) ->NSString {
        let t = temp.floatValue
        let farenhite = ((t - 273.15) * 1.80) + 32
        self.farenhiet = farenhite.description
        return farenhite.description
    }
    
    func cacheWeatherJSONFile(json:NSData){
        
        let destinationPath = returnNSURLForCachedData()
        json.writeToURL(destinationPath, atomically: true)
        
    }
    
    func returnNSURLForCachedData() -> NSURL {
        
        let fileMgr = NSFileManager.defaultManager()
        
        let documentDirectoryPath : String = NSSearchPathForDirectoriesInDomains(.DocumentDirectory,.UserDomainMask,true)[0] as! String
        let filePath = documentDirectoryPath + "/weatherJSON"
        var destinationPath = NSURL(fileURLWithPath: filePath)
//          println("\(destinationPath!)")
        
        return destinationPath!
    }
}
